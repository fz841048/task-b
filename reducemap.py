import pandas as pd
from collections import defaultdict
from threading import Thread

def map_function(input_data):
    # Map function that processes each record from input data. Emits (Passenger ID, 1) for each flight record. 
    mapped_data = []
    for record in input_data:
        try:
            passenger_id = record['Passenger ID']
            mapped_data.append((passenger_id, 1))
        except KeyError:
            # Handle case where 'Passenger ID' is missing or input data is malformed.
            continue
    return mapped_data

def shuffle_and_sort(mapped_data):
    # Shuffle and sort phase to simulate the grouping by key.
    sorted_data = defaultdict(list)
    for key, value in mapped_data:
        sorted_data[key].append(value)
    return dict(sorted_data)

def reduce_function(shuffled_data):
    # Reduce function that aggregates the count of flights for each passenger.
    reduced_data = defaultdict(int)
    for passenger_id, counts in shuffled_data.items():
        reduced_data[passenger_id] = sum(counts)
    return reduced_data

def mapreduce(input_data):
    # The mapreduce function orchestrates the map and reduce process. In a distributed system, the following steps would be parallelized.
    # Map step
    mapped_data = map_function(input_data)
    
    # Shuffle and sort phase
    shuffled_data = shuffle_and_sort(mapped_data)
    
    # Reduce step
    reduced_data = reduce_function(shuffled_data)
    
    return reduced_data

def read_csv_parallel(files):

    # Reads CSV files in parallel using threads. Each thread reads a file and returns a DataFrame. This is also in taskb.py, but put in here just in case.
    threads = []
    dfs = []

    def read_csv(file):
        df = pd.read_csv(file)
        dfs.append(df)

    for file in files:
        thread = Thread(target=read_csv, args=(file,))
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    return dfs

def main():
    # Paths to the CSV files
    airports_data_path = '/mnt/data/Top30_airports_LatLong.csv'
    passenger_data_path = '/mnt/data/AComp_Passenger_data_no_error.csv'
    
    # Read the data in parallel
    airports_data, passenger_data = read_csv_parallel([airports_data_path, passenger_data_path])
    
    # Assuming that the data cleaning and validation are done here
    # WIP

    # Convert the DataFrame to a list of dictionaries for the MapReduce process
    passenger_records = passenger_data.to_dict('records')

    # Execute the MapReduce
    reduced_output = mapreduce(passenger_records)

    # Find the top passenger by flights
    top_passenger = max(reduced_output.items(), key=lambda item: item[1])
    print(f"The top passenger by number of flights is: {top_passenger}")

if __name__ == "__main__":
    main()
